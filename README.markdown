DocileWalnut's Dotfiles
=======================

Installation
-----------------------
Well, I'd like it to use Rake, I guess. And some git cloning. For now though, nothing at all! Nothing at all! nothing at all!

The assumption is that you're cloning the dotfiles into ~/dotfiles. Replace that where different.

Bash
-----------
First is all the bash setup.

### ~/.bashrc ###
`. ~/dotfiles/bashrc`

### ~/.bash_profile ###
    if [ -f ~/.bashrc ]; then
      source ~/.bashrc
    fi


Vim
-------------


### .vim ###
`ln -s ~/dotfiles/vim ~/.vim`  
Symbolic link because this motha be a directory, son.

### .vimrc ###
`ln ~/dotfiles/vim/vimrc ~/.vimrc`

### .gvimrc ###
`ln ~/dotfiles/vim/gvimrc ~/.gvimrc`



Git
------------

### vcprompt ###
Dotfiles doesn't include a vcprompt binary because it always breaks. Download it and make it executable.
`curl -sL https://github.com/djl/vcprompt/raw/master/bin/vcprompt > ~/bin/vcprompt`
`chmod 755 ~/bin/vcprompt`

### .gitignore ###
`git config --global core.excludesfile ~/bin/dotfiles/.gitignore_global`
