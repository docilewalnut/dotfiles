if empty(glob('~/.vim/autoload/plug.vim'))
  silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin()




Plug 'itchyny/lightline.vim'
Plug 'niklaas/lightline-gitdiff'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'sheerun/vim-polyglot'

" Toggle lines into comments
Plug 'tpope/vim-commentary'

" Plug 'StanAngeloff/php.vim'

" Navigate quickly
Plug 'easymotion/vim-easymotion'

" Generate a minimap of code to show in sidebar
Plug 'wfxr/minimap.vim'

" Plug 'neoclide/coc.nvim', {'branch': 'release'}
" Plug 'yaegassy/coc-intelephense', {'do': 'yarn install --frozen-lockfile'}
" Plug 'phpactor/phpactor', {'for': 'php', 'tag': '*', 'do': 'composer install --no-dev -o'}
Plug 'liuchengxu/vista.vim'

Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
" Plug 'wellle/context.vim'

Plug 'rizzatti/dash.vim'

" Nvim only
if has("nvim")

    " Show nvim scrollbars with ascii chars
    Plug 'Xuyuanp/scrollbar.nvim'

    " Pretty icons for nvim-tree
    Plug 'nvim-tree/nvim-web-devicons' 

    " File explorer sidebar, like NERDTree
    Plug 'nvim-tree/nvim-tree.lua'

    " Show git symbols in gutter
    Plug 'lewis6991/gitsigns.nvim'

    " Tab bar indicators
    Plug 'akinsho/bufferline.nvim', { 'tag': '*' }

    " Help for remembering keybindings
    Plug 'folke/which-key.nvim'

    " Dashboard when opening nvim with shortcuts
    Plug 'glepnir/dashboard-nvim'

    " Syntax analysis and better highlighting
    Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}

    " Lightline replacement - show status/icons at bottom of buffer
    Plug 'nvim-lualine/lualine.nvim'

    " Better folding for code than builtin nvim folds
    Plug 'kevinhwang91/nvim-ufo' |
      \ Plug 'kevinhwang91/promise-async'

    function! UpdateRemotePlugins(...)
        " Needed to refresh runtime files
        let &rtp=&rtp
        UpdateRemotePlugins
    endfunction

    Plug 'catppuccin/nvim', { 'as': 'catpuccin' }

    Plug 'romgrk/fzy-lua-native'
    Plug 'nvim-lua/lsp-status.nvim'
    Plug 'neovim/nvim-lspconfig'

    " Show context line on top of where the cursor selection is
    Plug 'utilyre/barbecue.nvim'

    " Dependency for barbecue
    Plug 'SmiteshP/nvim-navic'

    " Add indicators to show level of indentation
    Plug 'lukas-reineke/indent-blankline.nvim'
    Plug 'HiPhish/rainbow-delimiters.nvim'

    " Lua functions that are used elsewhere
    Plug 'nvim-lua/plenary.nvim'

    " Find files, grep current directory, search project symbols
    Plug 'nvim-telescope/telescope.nvim', { 'tag': '0.1.8' }

    " FZF integration with telescope
    Plug 'nvim-telescope/telescope-fzf-native.nvim'

    " cmp autocomplete
    Plug 'hrsh7th/cmp-nvim-lsp'
    Plug 'hrsh7th/cmp-buffer'
    Plug 'hrsh7th/cmp-path'
    Plug 'hrsh7th/cmp-cmdline'
    Plug 'hrsh7th/nvim-cmp'

    " cmp sources
    Plug 'hrsh7th/cmp-vsnip'
    Plug 'hrsh7th/vim-vsnip'
    Plug 'hrsh7th/vim-vsnip-integ'
    Plug 'rafamadriz/friendly-snippets'
    Plug 'chrisgrieser/cmp-nerdfont'
    Plug 'tzachar/cmp-fuzzy-path'
    Plug 'tzachar/fuzzy.nvim'
    Plug 'petertriho/cmp-git'
    Plug 'uga-rosa/cmp-dictionary'
    Plug 'onsails/lspkind.nvim'

    " fix statuscolumn, indent markers, gitsigns, etc
    Plug 'luukvbaal/statuscol.nvim'

else 

    Plug 'itchyny/vim-gitbranch'
    Plug 'junegunn/fzf.vim'

    Plug 'catppuccin/vim', { 'as': 'catpuccin' }

    " NerdTree
    Plug 'preservim/nerdtree' |
      \ Plug 'Xuyuanp/nerdtree-git-plugin' |
      \ Plug 'ryanoasis/vim-devicons' | 
      \ Plug 'tiagofumo/vim-nerdtree-syntax-highlight'

    Plug 'airblade/vim-gitgutter'

endif


" Themes
Plug 'pineapplegiant/spaceduck', { 'branch': 'main' }
Plug 'hzchirs/vim-material'
Plug 'embark-theme/vim', { 'as': 'embark', 'branch': 'main' }
Plug 'NLKNguyen/papercolor-theme'
Plug 'sickill/vim-monokai', { 'as': 'monokai' }
Plug 'ghifarit53/tokyonight-vim'
Plug 'jsit/toast.vim'
Plug 'joshdick/onedark.vim'
Plug 'projekt0n/github-nvim-theme', { 'tag': 'v0.0.7' }
Plug 'RRethy/vim-illuminate'



" LSP configurations for vim-lsp
" if executable('gopls')
"     autocmd User lsp_setup call lsp#register_server({
"         \   'name': 'gopls',
"         \   'cmd': ['gopls'],
"         \   'allowlist': ['go', 'gomod'],
"         \ })
" endif

" Set 'vim-lsp' linter
" let g:ale_linters = {
"     \   'js': ['eslint'], " vim-lsp is implicitly active
"     \ }





set encoding=UTF-8


call plug#end()
