""""""""""""""""""""""""""""""
" => nvim Setup
""""""""""""""""""""""""""""""
if has("nvim")

    source ~/.config/nvim/gitsigns.lua
    nnoremap <silent> <expr> <Plug>(Gitsigns next_hunk) &diff ? ']c' : ":\<C-U>execute v:count1 . 'Gitsigns next_hunk'\<CR>"
    nnoremap <silent> <expr> <Plug>(Gitsigns prev_hunk) &diff ? '[c' : ":\<C-U>execute v:count1 . 'Gitsigns prev_hunk'\<CR>"

    source ~/.config/nvim/vsnip.lua
    source ~/.config/nvim/cmp.lua
    source ~/.config/nvim/lspconfig.lua
    source ~/.config/nvim/lspkind.lua
    source ~/.config/nvim/navic.lua
    source ~/.config/nvim/barbecue.lua
    source ~/.config/nvim/lualine.lua
    source ~/.config/nvim/bufferline.lua
    source ~/.config/nvim/treesitter.lua
    source ~/.config/nvim/telescope.lua
    source ~/.config/nvim/blankline.lua
    source ~/.config/nvim/ufo.lua
    source ~/.config/nvim/nvim-tree.lua
    source ~/.config/nvim/dashboard.lua
    source ~/.config/nvim/illuminate.lua
    source ~/.config/nvim/statuscol.lua

    map <leader>nn :NvimTreeToggle<cr>
    map <leader>nf :NvimTreeFindFile<cr>

    source ~/.config/nvim/keybinds.lua

else 
    source ~/.vim_runtime/vimrcs/NERDTree.vim

    """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
    " => Git gutter (Git diff)
    """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
    let g:gitgutter_enabled = 1
    let g:gitgutter_max_signs = 1000

    func! ToggleHighlightGutter()
        execute "GitGutterLineHighlightsToggle"
    endfunc
    nnoremap <silent> <leader>d :call ToggleHighlightGutter()<cr>


    hi GitGutterChangeLine ctermbg=94

    " Add (Neo)Vim's native statusline support.
    " NOTE: Please see `:h coc-status` for integrations with external plugins that
    " provide custom statusline: lightline.vim, vim-airline.
    set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

endif




""""""""""""""""""""""""""""""
" => bufExplorer plugin
""""""""""""""""""""""""""""""
let g:bufExplorerDefaultHelp=0
let g:bufExplorerShowRelativePath=1
let g:bufExplorerFindActive=0
let g:bufExplorerSortBy='name'
map <leader>o :BufExplorer<cr>


""""""""""""""""""""""""""""""
" => MRU plugin
""""""""""""""""""""""""""""""
let MRU_Max_Entries = 400
map <leader>f :MRU<CR>


""""""""""""""""""""""""""""""
" => YankStack
""""""""""""""""""""""""""""""
nmap <c-p> <Plug>yankstack_substitute_older_paste
nmap <c-P> <Plug>yankstack_substitute_newer_paste


""""""""""""""""""""""""""""""
" => CTRL-P
""""""""""""""""""""""""""""""
let g:ctrlp_working_path_mode = 0

"let g:ctrlp_map = '<c-f>'
" nmap <c-g> :Vista finder vim_lsp<cr>

if !has("nvim")
    let g:fzf_preview_window = ['up:60%']
    let g:fzf_colors =
    \ { 'fg':      ['fg', 'Folded'],
      \ 'bg':      ['bg', 'Normal'],
      \ 'hl':      ['fg', 'Constant'],
      \ 'fg+':     ['fg', 'IncSearch', 'CursorColumn', 'Normal'],
      \ 'bg+':     ['bg', 'CursorLine', 'CursorColumn'],
      \ 'hl+':     ['fg', 'Character'],
      \ 'info':    ['fg', 'SignColumn'],
      \ 'border':  ['fg', 'Conceal'],
      \ 'prompt':  ['fg', 'SignColumn'],
      \ 'pointer': ['fg', "Conditional"],
      \ 'marker':  ['fg', 'Keyword'],
      \ 'spinner': ['fg', 'Label'],
      \ 'header':  ['fg', 'Comment'] }

    if exists('$TMUX')
        let g:fzf_layout = { 'tmux': '-p90%,90%' }
    else
        let g:fzf_layout = { 'window': { 'width': 0.9, 'height': 0.8 } }
    endif

    function! RipgrepFzf(query, fullscreen)
      let command_fmt = 'RIPGREP_CONFIG_PATH="" rg --column --line-number --no-heading --color=always --smart-case -- %s || true'
      let initial_command = printf(command_fmt, shellescape(a:query))
      let reload_command = printf(command_fmt, '{q}')
      let spec = {'options': ['--phony', '--query', a:query, '--bind', 'change:reload:'.reload_command]}
      call fzf#vim#grep(initial_command, 1, fzf#vim#with_preview(spec), a:fullscreen)
    endfunction

    command! -nargs=* -bang RG call RipgrepFzf(<q-args>, <bang>0)
    " map <c-F> callRipgrepFzf()
endif


""""""""""""""""""""""""""""""
" => ZenCoding
""""""""""""""""""""""""""""""
" Enable all functions in all modes
let g:user_zen_mode='a'


""""""""""""""""""""""""""""""
" => snipMate (beside <TAB> support <CTRL-j>)
""""""""""""""""""""""""""""""
ino <c-j> <c-r>=snipMate#TriggerSnippet()<cr>
snor <c-j> <esc>i<right><c-r>=snipMate#TriggerSnippet()<cr>


""""""""""""""""""""""""""""""
" => Vim grep
""""""""""""""""""""""""""""""
let Grep_Skip_Dirs = 'RCS CVS SCCS .svn generated'
set grepprg=/bin/grep\ -nH


set foldmethod=expr
  \ foldexpr=lsp#ui#vim#folding#foldexpr()
  \ foldtext=lsp#ui#vim#folding#foldtext()



"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => vim-multiple-cursors
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:multi_cursor_next_key="\<C-s>"


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => surround.vim config
" Annotate strings with gettext http://amix.dk/blog/post/19678
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
vmap Si S(i_<esc>f)
au FileType mako vmap Si S"i${ _(<esc>2f"a) }<esc>


""""
" => Vista
"""""
map <leader>o :Vista!!<cr>
let g:vista_sidebar_width = 50

" let g:vista_icon_indent = ["  ", "  "]  "   ["╰─▸ ", "├─▸ "]
let g:vista#renderer#enable_icon = 1
let g:vista_fzf_preview = ['right:50%']
let g:vista_keep_fzf_colors = 1

if has("nvim") 
    let g:vista_default_executive = 'nvim_lsp'
    " let g:vista_executive_for = {
    "   \ 'js': 'nvim_lsp',
    "   \ }
else 
    let g:vista_default_executive = 'vim_lsp'
endif

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => lightline
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
if !has("nvim")

    let g:lightline#gitdiff#show_empty_indicators = 1
    let g:lightline#gitdiff#indicator_added = " "
    let g:lightline#gitdiff#indicator_deleted = " "
    let g:lightline#gitdiff#indicator_modified = " "
    let g:lightline#gitdiff#separator = ' '

    let g:lightline = {
          \ 'enable': { 'tabline': 0 },
          \ 'colorscheme': 'tokyonight',
          \ 'active': {
          \   'left': [ ['mode', 'paste'],
          \             ['fugitive', 'readonly', 'filename', 'modified'], 
          \             ['gitdiff', 'method'] 
          \   ],
          \   'right': [ ['gitbranch'], [ 'lineinfo' ] ]
          \ },
          \ 'inactive': {
          \   'left': [ ['mode' ], 
          \             ['readonly', 'filename', 'modified'], 
          \             ['gitdiff'] 
          \   ],
          \   'right': [ [] ]
          \ },
          \ 'component': {
          \   'readonly': '%{&filetype=="help"?"":&readonly?"🔒":""}',
          \   'modified': '%{&filetype=="help"?"":&modified?"+":&modifiable?"":"-"}',
          \   'fugitive': '%{exists("*fugitive#head")?fugitive#head():""}'
          \ },
          \ 'component_expand': {
          \   'gitdiff': 'lightline#gitdiff#get',
          \ },
          \ 'component_type': {
          \   'gitdiff': 'middle',
          \ },
          \ 'component_function': {
          \   'method': 'NearestMethodOrFunction',
          \   'gitbranch': 'LightlineBranch',
          \   'filename': 'LightlineFilename',
          \   'mode': 'LightlineMode'
          \ },
          \ 'component_visible_condition': {
          \   'readonly': '(expand("%:t")!~"NvimTree" && &filetype!="help"&& &readonly)',
          \   'filename': '(expand("%:t")!~"NvimTree")',
          \   'gitbranch': '(expand("%:t")!~"NvimTree")',
          \   'modified': '(expand("%:t")!~"NvimTree"&& &filetype!="help"&&(&modified||!&modifiable))',
          \   'fugitive': '(exists("*fugitive#head") && ""!=fugitive#head())'
          \ },
          \ 'separator': { 'left': '', 'right': ' ' },
          \ 'subseparator': { 'left': ' ', 'right': ' ' }
          \ }

    function! NearestMethodOrFunction() abort
      return get(b:, 'vista_nearest_method_or_function', '')
    endfunction

    function! LightlineBranch() 
      return " " . gitbranch#name()
    endfunction

    function! LightlineFilename()
      return &filetype ==# 'vimfiler' ? vimfiler#get_status_string() :
            \ &filetype ==# 'unite' ? unite#get_status_string() :
            \ &filetype ==# 'vimshell' ? vimshell#get_status_string() :
            \ expand('%:t') !=# '' ? split(expand('%:p'), '/')[-2] . '/' . split(expand('%:p'), '/')[-1] : '[No Name]'
    endfunction

    function! LightlineMode()
      return expand('%:t') =~# '^__Tagbar__' ? 'Tagbar':
            \ expand('%:t') ==# 'ControlP' ? 'CtrlP' :
            \ &filetype ==# 'unite' ? 'Unite' :
            \ &filetype ==# 'vimfiler' ? 'VimFiler' :
            \ &filetype ==# 'vimshell' ? 'VimShell' :
            \ lightline#mode() =~# 'NORMAL' ? '' : 
            \ lightline#mode() =~# 'COMMAND' ? '' :
            \ lightline#mode() =~# 'VISUAL' ? '' : 
            \ lightline#mode() =~# 'V-LINE' ? '' : 
            \ lightline#mode() =~# 'INSERT' ? '' : 
            \ lightline#mode() =~# 'REPLACE' ? '' : 
            \ lightline#mode()
    endfunction

else 
    let g:lightline = {
          \ 'enable': { 'tabline': 0, 'statusline' : 0  },
          \ } 
endif

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Vimroom
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:goyo_width=100
let g:goyo_margin_top = 2
let g:goyo_margin_bottom = 2
nnoremap <silent> <leader>z :Goyo<cr>


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Vim-go
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:go_fmt_command = "goimports"


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Syntastic (syntax checker)
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Python
let g:syntastic_python_checkers=['pyflakes']

" Javascript
let g:syntastic_javascript_checkers = ['jshint']

" Go
let g:syntastic_auto_loc_list = 1
let g:syntastic_go_checkers = ['go', 'golint', 'errcheck']

" Custom CoffeeScript SyntasticCheck
func! SyntasticCheckCoffeescript()
    let l:filename = substitute(expand("%:p"), '\(\w\+\)\.coffee', '.coffee.\1.js', '')
    execute "tabedit " . l:filename
    execute "SyntasticCheck"
    execute "Errors"
endfunc
nnoremap <silent> <leader>c :call SyntasticCheckCoffeescript()<cr>





"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => indentLine 
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:indentLine_chat = '│'




""""""""""""""""""""""""
" Ripgrep
""""""""""""""""""""""""

"Search for foo in current working directory: :grep foo.
"Search for foo in files under src/: :grep foo src.
"Search for foo in current file directory: :grep foo %:h1.
"Search for foo in current file directory’s parent directory: :grep foo %:h:h (and so on).
if executable("rg")
  set grepprg=rg\ --vimgrep\ --smart-case\ --hidden
  set grepformat=%f:%l:%c:%m
endif



" COC


" Don't pass messages to |ins-completion-menu|.
set shortmess+=c

" Always show the signcolumn, otherwise it would shift the text each time
" diagnostics appear/become resolved.
if has("nvim-0.5.0") || has("patch-8.1.1564")
  " Recently vim can merge signcolumn and number column into one
  set signcolumn=number
else
  set signcolumn=yes
endif

" Use tab for trigger completion with characters ahead and navigate.
" NOTE: Use command ':verbose imap <tab>' to make sure tab is not mapped by
" other plugin before putting this into your config.
" inoremap <silent><expr> <TAB>
"       \ coc#pum#visible() ? coc#pum#next(1):
"       \ <SID>check_back_space() ? "\<TAB>" :
"       \ coc#refresh()
" inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

" Use <c-space> to trigger completion.
if has('nvim')
  " inoremap <silent><expr> <c-space> coc#refresh()
else
  inoremap <silent><expr> <c-@> coc#refresh()
endif

" Make <CR> auto-select the first completion item and notify coc.nvim to
" format on enter, <cr> could be remapped by other vim plugin
" inoremap <silent><expr> <CR> coc#pum#visible() ? coc#pum#confirm()
"                               \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

" Use `[g` and `]g` to navigate diagnostics
" Use `:CocDiagnostics` to get all diagnostics of current buffer in location list.
" nmap <silent> [g <Plug>(coc-diagnostic-prev)
" nmap <silent> ]g <Plug>(coc-diagnostic-next)

" Telescope handles these in nvim
if has("nvim")
    " set coc_suggest_disable = 1
else
    nmap <silent> gd <Plug>(coc-definition)
    nmap <silent> gy <Plug>(coc-type-definition)
    nmap <silent> gi <Plug>(coc-implementation)
    nmap <silent> gr <Plug>(coc-references)
endif

" Use K to show documentation in preview window.
" nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if CocAction('hasProvider', 'hover')
    call CocActionAsync('doHover')
  else
    call feedkeys('K', 'in')
  endif
endfunction

" Highlight the symbol and its references when holding the cursor.
" autocmd CursorHold * silent call CocActionAsync('highlight')

" Symbol renaming.
nmap <leader>rn <Plug>(coc-rename)

" Formatting selected code.
xmap <leader>f  <Plug>(coc-format-selected)
nmap <leader>f  <Plug>(coc-format-selected)

augroup mygroup
  autocmd!
  " Setup formatexpr specified filetype(s).
  " autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  " Update signature help on jump placeholder.
  " autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

" Applying codeAction to the selected region.
" Example: `<leader>aap` for current paragraph
xmap <leader>a  <Plug>(coc-codeaction-selected)
nmap <leader>a  <Plug>(coc-codeaction-selected)

" Remap keys for applying codeAction to the current buffer.
nmap <leader>ac  <Plug>(coc-codeaction)
" Apply AutoFix to problem on the current line.
nmap <leader>qf  <Plug>(coc-fix-current)

" Run the Code Lens action on the current line.
nmap <leader>cl  <Plug>(coc-codelens-action)

" Map function and class text objects
" NOTE: Requires 'textDocument.documentSymbol' support from the language server.
xmap if <Plug>(coc-funcobj-i)
omap if <Plug>(coc-funcobj-i)
xmap af <Plug>(coc-funcobj-a)
omap af <Plug>(coc-funcobj-a)
xmap ic <Plug>(coc-classobj-i)
omap ic <Plug>(coc-classobj-i)
xmap ac <Plug>(coc-classobj-a)
omap ac <Plug>(coc-classobj-a)

" Remap <C-f> and <C-b> for scroll float windows/popups.
if has('nvim-0.4.0') || has('patch-8.2.0750')
  " nnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
  " nnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
  " inoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(1)\<cr>" : "\<Right>"
  " inoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(0)\<cr>" : "\<Left>"
  " vnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
  " vnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
endif

" Use CTRL-S for selections ranges.
" Requires 'textDocument/selectionRange' support of language server.
nmap <silent> <C-s> <Plug>(coc-range-select)
xmap <silent> <C-s> <Plug>(coc-range-select)

" Add `:Format` command to format current buffer.
" command! -nargs=0 Format :call CocActionAsync('format')

" Add `:Fold` command to fold current buffer.
" command! -nargs=? Fold :call     CocAction('fold', <f-args>)

" Add `:OR` command for organize imports of the current buffer.
" command! -nargs=0 OR   :call     CocActionAsync('runCommand', 'editor.action.organizeImport')


" Mappings for CoCList
" Show all diagnostics.
nnoremap <silent><nowait> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions.
nnoremap <silent><nowait> <space>e  :<C-u>CocList extensions<cr>
" Show commands.
nnoremap <silent><nowait> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document.
nnoremap <silent><nowait> <space>o  :<C-u>CocList outline<cr>
" Search workspace symbols.
nnoremap <silent><nowait> <space>s  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
nnoremap <silent><nowait> <space>j  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent><nowait> <space>k  :<C-u>CocPrev<CR>
" Resume latest coc list.
nnoremap <silent><nowait> <space>p  :<C-u>CocListResume<CR>


set fillchars+=foldopen:
set fillchars+=foldsep:░
set fillchars+=foldclose:


" let g:EasyMotion_do_mapping = 0 " Disable default mappings

" Jump to anywhere you want with minimal keystrokes, with just one key binding.
" `s{char}{label}`
nmap s <Plug>(easymotion-overwin-f)
" or
" `s{char}{char}{label}`
" Need one more keystroke, but on average, it may be more comfortable.
" nmap s <Plug>(easymotion-overwin-f2)

" Turn on case-insensitive feature
let g:EasyMotion_smartcase = 1

" JK motions: Line motions
map <Leader>j <Plug>(easymotion-j)
map <Leader>k <Plug>(easymotion-k)

" Use easymotion for inc search
" map  / <Plug>(easymotion-sn)
" omap / <Plug>(easymotion-tn)
" map  n <Plug>(easymotion-next)
" map  N <Plug>(easymotion-prev)


" if executable('intelephense')
"   augroup LspPHPIntelephense
"     au!
"     au User lsp_setup call lsp#register_server({
"         \ 'name': 'intelephense',
"         \ 'cmd': {server_info->[&shell, &shellcmdflag, 'intelephense --stdio']},
"         \ 'whitelist': ['php'],
"         \ 'initialization_options': {'storagePath': '/tmp/intelephense'},
"         \ 'workspace_config': {
"         \   'intelephense': {
"         \     'files': {
"         \       'maxSize': 1000000,
"         \       'associations': ['*.php', '*.phtml'],
"         \       'exclude': [],
"         \     },
"         \     'completion': {
"         \       'insertUseDeclaration': v:true,
"         \       'fullyQualifyGlobalConstantsAndFunctions': v:false,
"         \       'triggerParameterHints': v:true,
"         \       'maxItems': 100,
"         \     },
"         \     'format': {
"         \       'enable': v:true
"         \     },
"         \   },
"         \ }
"         \})
"   augroup END
" endif


let g:material_style = 'oceanic'



" Minimap
let g:minimap_width = 10
let g:minimap_auto_start = 0
let g:minimap_auto_start_win_enter = 1
let g:minimap_git_colors = 1
map <leader>m :MinimapToggle<cr>

let g:minimap_highlight_search = 1
let g:minimap_highlight_range = 1
let g:minimap_base_highlight = 'minimapNormal'
let g:minimap_search_color = 'minimapSearch'

" Scrollbar 
augroup ScrollbarInit
  autocmd!
  autocmd WinScrolled,VimResized,QuitPre * silent! lua require('scrollbar').show()
  autocmd WinEnter,FocusGained           * silent! lua require('scrollbar').show()
  autocmd WinLeave,BufLeave,BufWinLeave,FocusLost            * silent! lua require('scrollbar').clear()
augroup end

let g:scrollbar_winblend = 70
let g:scrollbar_min_size = 5
let g:scrollbar_right_offset = 1 
let g:scrollbar_shape = {
    \ 'head':  '',
    \ 'body':  '█',
    \ 'tail':  '',
    \}

let g:scrollbar_highlight = {
    \ 'body': 'ScrollbarBody'
    \ }


let g:onedark_terminal_italics = 1
