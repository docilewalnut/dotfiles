map <leader>nn :NERDTreeToggle<cr>
map <leader>nb :NERDTreeFromBookmark
map <leader>nf :NERDTreeFind<cr>


let g:NERDTreeWinPos = "left"
let NERDTreeShowHidden=0
let NERDTreeIgnore = ['\.pyc$', '__pycache__']
let g:NERDTreeWinSize=35
let g:NERDTreeGitStatusUseNerdFonts = 1
let g:NERDTreeGitStatusShowIgnored = 1
let g:NERDTreeGitStatusShowClean = 1

let g:NERDTreeGitStatusIndicatorMapCustom = {
                \ 'Dirty'     :'✹',
                \ 'Clean'     :' ',
                \ }

" Start NERDTree when Vim is started without file arguments.
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists('s:std_in') | NERDTree | wincmd p | endif

" If another buffer tries to replace NERDTree, put it in the other window, and bring back NERDTree.
autocmd BufEnter * if bufname('#') =~ 'NERD_tree_\d\+' && bufname('%') !~ 'NERD_tree_\d\+' && winnr('$') > 1 |
    \ let buf=bufnr() | buffer# | execute "normal! \<C-W>w" | execute 'buffer'.buf | endif

" Open the existing NERDTree on each new tab.
autocmd BufWinEnter * if getcmdwintype() == '' | silent NERDTreeMirror | endif

" Close the tab if NERDTree is the only window remaining in it.
autocmd BufEnter * if winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif

" Check if NERDTree is open or active
function! IsNERDTreeOpen()        
  return exists("t:NERDTreeBufName") && (bufwinnr(t:NERDTreeBufName) != -1)
endfunction

" Call NERDTreeFind if NERDTree is active, current window contains a modifiable
" file, and we're not in vimdiff
function! SyncTree()
  if &modifiable && IsNERDTreeOpen() && strlen(expand('%')) > 0 && !&diff
    NERDTreeFind
    wincmd p
  endif
endfunction

" Highlight currently open buffer in NERDTree
autocmd BufEnter * call SyncTree()

" Switch cursorline in NerdTree
autocmd BufEnter * call ToggleCursorLine()

function! ToggleCursorLine()
    if (bufname("%") =~ "NERD_Tree_")
        highlight CursorLine term=underline ctermbg=238 guibg=#333333 ctermfg=172 guifg=#CC8A3A
    else
        highlight CursorLine term=underline ctermbg=237 guibg=#222222
    endif
endfunction


let g:WebDevIconsNerdTreeAfterGlyphPadding = '  '
let g:WebDevIconsNerdTreeGitPluginForceVAlign = 1
let g:WebDevIconsUnicodeDecorateFolderNodes = 1


" Nerd 
highlight NERDTreeDirSlash guifg=#666666

highlight NERDTreeHelp guifg=#333333
highlight NERDTreeUp guifg=#666666
highlight NERDTreeCWD term=underline cterm=underline gui=underline guifg=#b3a1e6

highlight NERDTreeFile guifg=#DEDEDE
highlight NERDTreeFlags guifg=#DEDEDE
highlight NERDTreeLink guifg=#DEDEDE

" Disable lightline status on nerdtree windows 
" augroup filetype_nerdtree
"     au!
"     au FileType nerdtree call s:disable_lightline_on_nerdtree()
"     au WinEnter,BufWinEnter,TabEnter * call s:disable_lightline_on_nerdtree()
" augroup END

" fu s:disable_lightline_on_nerdtree() abort
"     let nerdtree_winnr = index(map(range(1, winnr('$')), {_,v -> getbufvar(winbufnr(v), '&ft')}), 'nerdtree') + 1
"     call timer_start(0, {-> nerdtree_winnr && setwinvar(nerdtree_winnr, '&stl', '%#Normal#')})
" endfu


" " Disable lightline status on nerdtree windows 
" augroup filetype_vista
"     au!
"     au FileType Vista call s:disable_lightline_on_vista()
"     au WinEnter,BufWinEnter,TabEnter * call s:disable_lightline_on_vista()
" augroup END


" fu s:disable_lightline_on_vista() abort
"     let vista_winnr = index(map(range(1, winnr('$')), {_,v -> getbufvar(winbufnr(v), '&ft')}), 'vista') + 1
"     call timer_start(0, {-> vista_winnr && setwinvar(vista_winnr, '&stl', '%#Normal#')})
" endfu

