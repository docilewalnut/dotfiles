" set number
" set numberwidth=7
set relativenumber
if has("nvim")
"     set relativenumber
    set signcolumn=auto:1
endif 
" set signcolumn=number
set cmdheight=0


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => GUI related
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Set font according to system
if has("mac") || has("macunix")
    set gfn=Hack:h14,Source\ Code\ Pro:h15,Menlo:h15
elseif has("win16") || has("win32")
    set gfn=Hack:h14,Source\ Code\ Pro:h12,Bitstream\ Vera\ Sans\ Mono:h11
elseif has("gui_gtk2")
    set gfn=Hack\ 14,Source\ Code\ Pro\ 12,Bitstream\ Vera\ Sans\ Mono\ 11
elseif has("linux")
    set gfn=Hack\ 14,Source\ Code\ Pro\ 12,Bitstream\ Vera\ Sans\ Mono\ 11
elseif has("unix")
    set gfn=Monospace\ 11
endif

" Disable scrollbars (real hackers don't use scrollbars for navigation!)
set guioptions-=r
set guioptions-=R
set guioptions-=l
set guioptions-=L

" Enable mouse
set mouse=a

" Colorscheme
set background=dark
if exists('+termguicolors')
    let &t_8f = "\<Esc>[38;2;%lu;%lu;%lum"
    let &t_8b = "\<Esc>[48;2;%lu;%lu;%lum"
    set termguicolors
endif

colorscheme tokyonight "" set before catpuccin for bufferline + nvim-tree
if has("nvim")
    colorscheme catppuccin-mocha
else
    colorscheme catpuccin_mocha
endif

" Ensure selections are bright
" highlight Visual term=reverse ctermfg=14 ctermbg=242 guifg=cyan guibg=#3e4452
highlight Search term=reverse ctermfg=15 ctermbg=60 guifg=#f2ce00 guibg=#686f9a
highlight Visual term=reverse ctermbg=242 guibg=#3e4452

highlight NvimTreeGitDirty gui=NONE cterm=NONE ctermfg=172 ctermbg=0 guifg=#00a3cc 
highlight NvimTreeFolderName ctermbg=0 ctermfg=172 guifg=#cccccc
highlight NvimTreeOpenedFolderName ctermbg=0 ctermfg=172 guifg=#cccccc
highlight NvimTreeEmptyFolderName ctermbg=0 ctermfg=172 guifg=#999999

" Cursor
set cursorline
" set cursorlineopt=number
highlight CursorLineNr guifg=#f2ce00
highlight CursorLineSign guifg=#f2ce00
" highlight CursorLine term=underline ctermbg=238 guibg=#333333 ctermfg=172 guifg=#cc8a3a

" Transparent background - kitty on Debian
highlight Normal ctermbg=NONE guibg=NONE
highlight NormalNC ctermbg=NONE guibg=NONE
highlight NvimTreeNormal ctermbg=NONE guibg=NONE
" highlight BufferlineFill ctermbg=none guibg=none
" highlight BufferLineSeparator ctermfg=none guifg=none
" highlight BufferLineSeparatorSelected ctermfg=none guifg=none
" highlight BufferLineSeparatorSelected ctermfg=none guifg=none


highlight CmpItemMenu term=none gui=none cterm=none guibg=#1e1e2f 
highlight CmpBorder term=none gui=none cterm=none guibg=none 
highlight CmpDocBorder term=none gui=None cterm=None guibg=#1e1e2f guifg=#1e1e2f 
highlight CmpItemAbbrMatch cterm=none gui=none guibg=none
highlight CmpItemAbbrMatchFuzzy cterm=none gui=none guibg=none

" Bright border for window separators
highlight VertSplit guifg=#45475b
highlight NvimTreeWinSeparator guifg=#45475b


" Buffer bar + separator
" highlight BufferLineFill guibg=#1b1d36 
" highlight BufferLineSeparator gui=none cterm=none guibg=#2f375f guifg=#1b1d36

" Inactive tabs
highlight BufferLineBackground gui=italic 
" highlight BufferLineDuplicate guibg=#2f375f
" highlight BufferLineDevIconLuaInactive gui=none guibg=#2f375f
" highlight BufferLineModified gui=none guibg=#2f375f
" highlight BufferLineInfo gui=none guibg=#2f375f

" Active tab
" highlight BufferLineDevIconLua gui=none guibg=#2f375f
" highlight BufferLineSeparatorSelected gui=none cterm=none guibg=#000000 guifg=#1B1D36
highlight BufferLineBufferSelected gui=italic cterm=NONE guifg=#05bdfd

" highlight ScrollbarBody gui=bold cterm=bold guibg=#67709A

highlight minimapSearch gui=bold guifg=#5bcb96
highlight minimapNormal guibg=normal guifg=#CCCCCC
highlight minimapRange guibg=#2F375F 
highlight minimapCursor guibg=#2F375F guifg=#e4bf02


highlight CocInfoVirtualText gui=bold guibg=#232433 guifg=#9ECD6A
highlight CocHintVirtualText gui=bold guibg=#232433 guifg=#AC91E6
highlight CocWarningVirtualText gui=bold guibg=#232433 guifg=#E1AE68
highlight CocErrorVirtualText gui=bold guibg=#232433 guifg=#DB687E

" Filename part of the lightline
" highlight LightLineLeft_active_1 ctermfg=14  ctermbg=237 guifg=#f2ce00 guibg=#30365F 
" highlight LightLineLeft_inactive_0 ctermfg=14  ctermbg=237 guifg=#f2ce00 guibg=#30365F 
" highlight LightLineLeft_inactive_1 ctermfg=14  ctermbg=237 guifg=#f2ce00 guibg=#30365F 

highlight Comment guifg=#6774c6


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Turn persistent undo on 
"    means that you can undo even when you close a buffer/VIM
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
try
    set undodir=~/.vim_runtime/temp_dirs/undodir
    set undofile
catch
endtry


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Command mode related
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Smart mappings on the command line
cno $h e ~/
cno $d e ~/Desktop/
cno $j e ./
cno $c e <C-\>eCurrentFileDir("e")<cr>

" $q is super useful when browsing on the command line
" it deletes everything until the last slash 
cno $q <C-\>eDeleteTillSlash()<cr>

" Bash like keys for the command line
cnoremap <C-A>		<Home>
cnoremap <C-E>		<End>
cnoremap <C-K>		<C-U>

cnoremap <C-P> <Up>
cnoremap <C-N> <Down>

" Map ½ to something useful
map ½ $
cmap ½ $
imap ½ $

if !has('nvim')
    " map <c-f> :FZF<cr>
    map <c-r> :RG<cr>
    map <c-g> :Vista finder vim_lsp<cr>
endif 

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Parenthesis/bracket
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
vnoremap $1 <esc>`>a)<esc>`<i(<esc>
vnoremap $2 <esc>`>a]<esc>`<i[<esc>
vnoremap $3 <esc>`>a}<esc>`<i{<esc>
vnoremap $$ <esc>`>a"<esc>`<i"<esc>
vnoremap $q <esc>`>a'<esc>`<i'<esc>
vnoremap $e <esc>`>a"<esc>`<i"<esc>

" Map auto complete of (, ", ', [
inoremap $1 ()<esc>i
inoremap $2 []<esc>i
inoremap $3 {}<esc>i
inoremap $4 {<esc>o}<esc>O
inoremap $q ''<esc>i
inoremap $e ""<esc>i


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => General abbreviations
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
iab xdate <c-r>=strftime("%d/%m/%y %H:%M:%S")<cr>


func! ToggleRelative() 
  if &rnu
    set nornu
  else 
    set rnu
  endif
endfunc
nnoremap <silent> <leader>r :call ToggleRelative()<cr>


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Omni complete functions
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
autocmd FileType css set omnifunc=csscomplete#CompleteCSS



"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Helper functions
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
func! DeleteTillSlash()
    let g:cmd = getcmdline()

    if has("win16") || has("win32")
        let g:cmd_edited = substitute(g:cmd, "\\(.*\[\\\\]\\).*", "\\1", "")
    else
        let g:cmd_edited = substitute(g:cmd, "\\(.*\[/\]\\).*", "\\1", "")
    endif

    if g:cmd == g:cmd_edited
        if has("win16") || has("win32")
            let g:cmd_edited = substitute(g:cmd, "\\(.*\[\\\\\]\\).*\[\\\\\]", "\\1", "")
        else
            let g:cmd_edited = substitute(g:cmd, "\\(.*\[/\]\\).*/", "\\1", "")
        endif
    endif   

    return g:cmd_edited
endfunc

func! CurrentFileDir(cmd)
    return a:cmd . " " . expand("%:p:h") . "/"
endfunc



"""""
" => Git commit settings
"""""
func! Gitcommit()
    set tabline=0
    set bufferline=0
endfunc
autocmd FileType gitcommit 
