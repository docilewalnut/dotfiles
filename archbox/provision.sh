# Provisioning is done as ROOT and NOT the vagrant user,
# so the ~ shorthand won't work, and must be defined explicitly
#
# NO TRAILING SLASH on this var, so we can do: 
# ${home}/.npm and not ${home}.npm, which looks weird
home=/home/vagrant


echo -e '\n----------------------------------------------------------------------\n'
echo -e "Installing CLI packages..."
echo -e '\n----------------------------------------------------------------------\n'
sudo pacman -S --noconfirm git tig vim ripgrep nodejs npm htop mc tmux  \
    zsh powerline-fonts fish tree cowsay lolcat fortune-mod 

echo -e '\n----------------------------------------------------------------------\n'
echo -e "Installing GUI packages..."
echo -e '\n----------------------------------------------------------------------\n'
sudo pacman -R --noconfirm virtualbox-guest-utils-nox
sudo pacman -S --noconfirm xorg-xinit xorg-server xorg-xlsfonts awesome \
    xclip xsel rxvt-unicode chromium virtualbox-guest-utils

echo -e "\n\n\n\nPackages installed!"
echo -e '\n----------------------------------------------------------------------\n'
echo "Installing global NPM packages..."
mkdir ${home}/.npm
npm -g install yarn n diff-so-fancy 

echo -e '\n----------------------------------------------------------------------\n'
echo "Generating SSH keys..." 
ssh-keygen -f ${home}/.ssh/id_rsa -t rsa -N ''

echo -e '\n----------------------------------------------------------------------\n'
echo "Configuratin'...."
echo -e '\n----------------------------------------------------------------------\n'
echo "Configuratin' /hosts file..." 
sudo echo "192.168.15.20   git.skrumble.local" >> /etc/hosts

echo -e '\n----------------------------------------------------------------------\n'

echo "Creating directories"
mkdir ${home}/dev && echo "["!:0"]"


echo "Installing fonts..."
cp -r ${home}/dotfiles/fonts/* /usr/share/fonts
chmod -R 0555 /usr/share/fonts/*
echo "Fonts installed:"
fc-list

echo -e '\n----------------------------------------------------------------------\n'
echo "Configuratin' git..."
ln -s ${home}/dotfiles/git/gitconfig ${home}/.gitconfig
ln -s ${home}/dotfiles/git/gitignore_global ${home}/.gitignore_global
ln -s ${home}/dotfiles/git/gitk ${home}/.gitk
mkdir -p ${home}/.config/git
ln -s ${home}/dotfiles/git/gitk ${home}/.config/git/gitk

echo -e '\n----------------------------------------------------------------------\n'
echo "Configuratin' zsh..."
mkdir -p ${home}/.config/oh-my-zsh/themes
su -c sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)" vagrant
cp ${home}/dotfiles/zsh/matty.zsh-theme ${home}/.oh-my-zsh/themes
rm -rf ${home}/.zshrc
ln -s ${home}/dotfiles/zsh/zshrc ${home}/.zshrc

echo -e '\n----------------------------------------------------------------------\n'
echo "Configuratin' fish..."
mkdir -p ${home}/.config/fish
sudo chown -R vagrant ${home}/.config/fish
ln -s ${home}/dotfiles/fish/config.fish ${home}/.config/fish
echo "/usr/bin/fish" | sudo tee -a /etc/shells
sudo chsh -s /usr/bin/fish vagrant
curl -L -s https://get.oh-my.fish > install --noninteractive
fish install --path=~/.local/share/omf --config=~/.config/omf

echo "Configuratin' tmux..."
ln -s ${home}/dotfiles/tmux/tmux.conf ${home}/.tmux.conf

echo "Configuratin' awesome..."
mkdir -p ${home}/.config/awesome/
ln -s ${home}/dotfiles/awesome/* ${home}/.config/awesome/
ln -s ${home}/dotfiles/x/Xresources ${home}/.Xresources

echo "Configuratin' vim..."
ln -s ${home}/dotfiles/vim ${home}/.vim_runtime
ln -s ${home}/dotfiles/vim/vimrc ${home}/.vimrc

echo -e '\n----------------------------------------------------------------------\n'
echo "Fixing permissions..."
sudo chown -R vagrant ${home}
