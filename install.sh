#!/bin/bash
echo "This will install tools and apps for MacOS"

mkdir -p ~/dotfiles/tmp

# Install the MacOS command-line tools like gcc
echo "Instaling xcode-tools like gcc"
if ! xcode-select -p; then
    xcode-select --install
fi

echo "Install brew and packaages? [Y/n]"
read install_brew

if [ "$install_brew" != "n" ] 
then

    echo "Installing brew..."
    /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"

    echo "Installing brew packages..."

    echo "Installing npm..."
    brew install npm

    echo "Installing zsh..."
    brew install zsh

    echo "Installing vim..."
    brew install vim

    echo "Installing tmux..."
    brew install tmux

    echo "Installing tmuxinator..."
    brew install tmuxinator

    echo "Installing tig..."
    brew install tig

    echo "Installing htop..."
    brew install htop

    echo "Installing mc..."
    brew install midnight-commander

    echo "Installing ripgrep..."
    brew install ripgrep

    echo "Installing lsd..."
    brew install lsd

    echo "Installing code-minimap"
    brew install code-minimap

    echo "Installing cowsay"
    brew install cowsay

    echo "Installing lolcat" 
    brew install lolcat 

    echo "Installing fortune" 
    brew install fortune

    echo "Installing neofetch..."
    brew install neofetch

    echo "Installing nvm..."
    brew install nvm

    echo "Installing rbenv"
    brew install rbenv ruby-build

    echo "Installing cocoapods"
    brew install cocoapods

fi

# install oh-my-zsh
echo "Install oh-my-zsh? [Y/n]"
read install_ohmyzsh

if [ "$install_ohmyzsh" != "n" ]
then
    echo "Installing oh-my-zsh"
    sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

    echo "Installing p10k theme for oh-my-zsh"
    git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k

    echo "Configuring oh-my-zsh..."
    ln -s ~/dotfiles/zsh/zshrc ~/.zshrc
fi

# install Fish
echo "Install Fish? [Y/n]"
read install_fish

if [ "$install_fish" != "n" ]
then
    echo "Installing fish"
    brew install fish

    echo "Installing oh-my-fish"
    curl -L https://get.oh-my.fish | fish

    echo "Setting fish as default"
    echo "/usr/local/bin/fish" | sudo tee -a /etc/shells
    chsh -s /usr/local/bin/fish

    echo "Symlinking custom config to ~/.config/fish"
    ln -s ~/dotfiles/fish/config.fish ~/.config/fish
fi


# install amix's awesome Vimrc
# echo "Install amix/vimrc? [Y/n]"
# read install_amixvimrc

# if [ "$install_amixvimrc" != "n" ]
# then
    # echo "Installing amix/vimrc into ~/.vim_runtime"
    # git clone https://github.com/amix/vimrc.git ~/.vim_runtime
    # sh ~/.vim_runtime/install_awesome_vimrc.sh

# fi

echo "Installing vim+nvim config"
ln -s ~/dotfiles/vim/ ~/.vim_runtime
ln -s ~/dotfiles/vim/vimrc ~/.vimrc
ln -s ~/dotfiles/nvim ~/.config/nvim

echo "Configuring tmux(inator)"
ln -s ~/dotfiles/tmux/tmux.conf ~/.tmux.conf
ln -s ~/dotfiles/tmux/tmuxinator ~/.tmuxinator

echo "Configuring git"
ln -s ~/dotfiles/git/gitconfig ~/.gitconfig
ln -s ~/dotfiles/git/gitignore_global ~/.gitignore_global
touch ~/dotfiles/git/gitconfig.user.inc

echo "Configuring gitk"
ln -s ~/dotfiles/git/gitk ~/.gitk
ln -s ~/dotfiles/git/gitk ~/.config/git/gitk

echo "Configuring tig"
ln -s ~/dotfiles/git/tigrc ~/.tigrc

echo "Configuring ripgrep"
ln -s ~/dotfiles/macOS/ripgreprc ~/.ripgreprc

echo "Configuring p10k"
ln -s ~/dotfiles/zsh/p10k/p10k.zsh ~/.p10k.zsh

# echo "Configuring Little Snitch"
# ln -s ~/bin/dotfiles/ "~/Library/Application Support/Little Snitch/configuration.user.xpl"

echo "Configuring Midnight Commander"
ln -s ~/dotfiles/mc/ini ~/.config/mc/ini
ln -s ~/dotfiles/mc/panels.ini ~/.config/mc/panels.ini


echo "Installing fonts"
cp -a ~/dotfiles/fonts/**/*.{ttf,otf} ~/Library/fonts
