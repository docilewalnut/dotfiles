-- disable netrw at the very start of your init.lua (strongly advised)
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1

local api = require("nvim-tree.api")

require("nvim-tree").setup({
  sort_by = "case_sensitive",
  hijack_unnamed_buffer_when_opening = false,
  update_cwd = true,
  update_focused_file = {
    enable = true,
    update_cwd = false,
  },
  git = {
    enable = true,
    show_on_dirs = true,
    show_on_open_dirs = false,
    ignore = true
  },
  view = {
    width = 30,
    -- mappings = {
    --   list = {
    --     { key = "u", action = "dir_up" },
    --   },
    -- },
  },
  filesystem_watchers = {
    enable = true
  },
  renderer = {
    group_empty = true,
    highlight_git = true,
    indent_markers = {
      enable = false,
      inline_arrows = true, 
      icons = {
        corner = "└", edge = "│", item = "├", bottom = "─", none = "",
      }
    },
    icons = {
      git_placement = "before", --signcolumn",
      show = {
        file = true,
        folder = true,
        folder_arrow = true,
        git = true,
      },
      glyphs = {
        folder = {
          arrow_closed = "",
          arrow_open = "",
          default = "",
          open = "",
          empty = "",
          empty_open = "",
          symlink = "",
          symlink_open = "",
        },
        git = {
          unstaged = "",
          staged = "",
          unmerged = "",
          renamed = "➜",
          untracked = "",
          deleted = "",
          ignored = "◌",
        }
      }
    },
  },
  filters = {
    dotfiles = true,
  },
})

vim.api.nvim_set_hl(0, 'NvimTreeGitDirty', { bg="#000000", fg="#00a3cc" })
vim.api.nvim_set_hl(0, 'NvimTreeFolderName', { bg="#000000", fg="#cccccc" })
vim.api.nvim_set_hl(0, 'NvimTreeOpenedFolderName', { bg="#000000", fg="#cccccc" })
vim.api.nvim_set_hl(0, 'NvimTreeEmptyFolderName', { bg="#000000", fg="#999999" })


-- Auto handler to determine if the nvim-tree should be opened
local function open_nvim_tree(data)

  -- buffer is a [No Name]
  local no_name = data.file == "" and vim.bo[data.buf].buftype == ""

  -- buffer is a directory
  local directory = vim.fn.isdirectory(data.file) == 1

  if not no_name and not directory then
    -- return
  end

  -- buffer is commit
  local gitcommit = vim.bo[data.buf].filetype == "gitcommit" 
  if gitcommit then
    return 
  end

  -- change to the directory
  if directory then
    vim.cmd.cd(data.file)
    api.tree.open({ focus = false })
    api.tree.toggle_hidden_filter()
    return
  end

  -- open the tree
  api.tree.open({ focus = false, find_file = true })
  api.tree.toggle_hidden_filter()
end
-- vim.api.nvim_create_autocmd({ "VimEnter" }, { callback = open_nvim_tree })


vim.api.nvim_create_autocmd("BufEnter", {
  nested = true,
  callback = function()
    if #vim.api.nvim_list_wins() == 1 and require("nvim-tree.utils").is_nvim_tree_buf() then
      vim.cmd "quit"
    end
  end
})
