-- triggers CursorHold event faster
vim.opt.updatetime = 200

require("barbecue").setup({
    show_navic = true,
    context_follow_icon_color = true,
    show_modified = true,
    symbols = {
        modified = "●",
        ellipsis = "…",
        separator = "/",
    },
    kinds = {
        File = " ",
        Module = " ",
        Namespace = " ",
        Package = " ",
        Class = " ",
        Method = " ",
        Property = "襁",
        Field = " ",
        Constructor = "略 ",
        Enum = " ",
        Interface = " ",
        Function = "",
        Variable = "",
        Constant = "",
        String = " ",
        Number = " ",
        Boolean = " ",
        Array = "[] ",
        Object = " ",
        Key = "",
        Null = "",
        EnumMember = "",
        Struct = "",
        Event = "",
        Operator = "",
        TypeParameter = "",
    },
    attach_navic = false,
    create_autocmd = false, -- prevent barbecue from updating itself automatically
})

vim.api.nvim_create_autocmd({
  "WinScrolled", -- or WinResized on NVIM-v0.9 and higher
  "BufWinEnter",
  "CursorHold",
  "InsertLeave",
  "LspAttach",

  -- include these if you have set `show_modified` to `true`
  "BufWritePost",
  "TextChanged",
  "TextChangedI",
}, {
  group = vim.api.nvim_create_augroup("barbecue.updater", {}),
  callback = function()
    require("barbecue.ui").update()
  end,
})

vim.api.nvim_create_autocmd("LspAttach", { 
 group = augroup, 
 callback = function(a) 
   local client = vim.lsp.get_client_by_id(a.data.client_id) 
    local navic = require('nvim-navic')
   if client.server_capabilities["documentSymbolProvider"] then navic.attach(client, a.buf) end 
 end, 
}) 

-- require("lspconfig")[server].setup({
--   on_attach = function(client, bufnr)
--     if client.server_capabilities["documentSymbolProvider"] then
--       require("nvim-navic").attach(client, bufnr)
--     end
--   end
-- })
