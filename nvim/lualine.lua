local navic = require("nvim-navic")

local diffOpts = {
    'diff', 
    symbols = {added = ' ', modified = ' ', removed = ' '},
    always_visible = true
}

require('lualine').setup({
  options = {
    extensions = {'symbols-outline', 'nvim-tree'},
    icons_enabled = true,
    theme = 'ayu_mirage',
    component_separators = { left = '', right = ''},
    section_separators = { left = '', right = ''},
    disabled_buftypes = {
        'commitmsg'
    },
    disabled_filetypes = {
      statusline = { "NvimTree", "vista_kind", "vista", "minimap" },
      winbar = { },
    },
    ignore_focus = {
      "NvimTree", "vista_kind", "vista", "minimap"
    },
    always_divide_middle = true,
    globalstatus = true,
    refresh = {
      statusline = 1000,
      tabline = 1000,
      winbar = 10,
    }
  },
  sections = {
    lualine_a = { {
      'mode',
      fmt = function(str)
        local mode_icons = {
            ['NORMAL'] = ' NRM' , 
            ['COMMAND'] = ' CMD' ,
            ['VISUAL'] = ' VIS' , 
            ['V-LINE'] = ' VLN' , 
            ['INSERT'] = ' INS' , 
            ['REPLACE'] = ' REP' , 
        }
        return mode_icons[str]
      end
    }},
    lualine_b = {{
      'filename',
      use_mode_colors = true,
      separator = { left = '', right = ''},
      icons_enabled = true,
      file_status = true,
      symbols = {
        modified = '+',
        readonly = '',
        unnamed = '[Untitled]',
        newfile = '[New]',
      },
      color = function(section)
        return { 
          fg = vim.bo.modified and 'BufferLineModified' or '#000000',
          bg = '#666666'
        }
      end
    }},
    lualine_c = { diffOpts},
    lualine_x = {'diagnostics'},
    lualine_y = {'filetype' },
    lualine_z = {'branch'}
  },
  inactive_sections = {
    lualine_a = {},
    lualine_b = {'filename'},
    lualine_c = { diffOpts, },
    lualine_x = {'diagnostics'},
    lualine_y = {'filetype'},
    lualine_z = {}
  },
  tabline = {},
  winbar = {},
  inactive_winbar = {},
})



vim.api.nvim_create_autocmd("LspAttach", { 
 group = augroup, 
 callback = function(a) 
   local client = vim.lsp.get_client_by_id(a.data.client_id) 
    local navic = require('nvim-navic')
   if client.server_capabilities["documentSymbolProvider"] then navic.attach(client, a.buf) end 
 end, 
}) 
