vim.opt.list = true
-- vim.opt.listchars:append "space: "
vim.opt.listchars:append "eol: "

local highlight = {
    "RainbowViolet",
    "RainbowCyan",
    "RainbowBlue",
    "RainbowGreen",
    "RainbowYellow",
    "RainbowOrange",
    "RainbowRed",
}
local hooks = require "ibl.hooks"

-- create the highlight groups in the highlight setup hook, so they are reset
-- every time the colorscheme changes
hooks.register(hooks.type.HIGHLIGHT_SETUP, function()
    vim.api.nvim_set_hl(0, "RainbowRed", { fg = "#E06C75" })
    vim.api.nvim_set_hl(0, "RainbowYellow", { fg = "#E5C07B" })
    vim.api.nvim_set_hl(0, "RainbowBlue", { fg = "#61AFEF" })
    vim.api.nvim_set_hl(0, "RainbowOrange", { fg = "#D19A66" })
    vim.api.nvim_set_hl(0, "RainbowGreen", { fg = "#98C379" })
    vim.api.nvim_set_hl(0, "RainbowViolet", { fg = "#C678DD" })
    vim.api.nvim_set_hl(0, "RainbowCyan", { fg = "#56B6C2" })
end)

vim.g.rainbow_delimiters = { highlight = highlight }

require("ibl").setup({
    enabled = true,
    indent = { 
        char = "▏",
        -- highlight = highlight 
    },
    scope = {
        highlight = highlight,
        enabled = true,
        show_start = true,
        show_end = true,
        priority = 500,
        char = "▏"
    },
    exclude = {
        filetypes = {
            "dashboard"
        }
    }
    -- whitespace = {},
})


hooks.register(hooks.type.SCOPE_HIGHLIGHT, hooks.builtin.scope_highlight_from_extmark)
hooks.register(hooks.type.SCOPE_HIGHLIGHT, hooks.builtin.scope_highlight_from_extmark)

vim.g.indent_blankline_show_first_indent_level = true
vim.g.indent_blankline_use_treesitter = true
vim.g.indent_blankline_show_current_context = true
vim.g.indent_blankline_context_patterns = { 'class', 'return', 'function', 'method', '^if', '^while', '^for', '^object', '^table', 'block', 'arguments', "declaration", "expression", "pattern", "primary_expression", "statement", "switch_body", 'if_statement', 'else_clause', 'jsx_element', 'try_statement', 'catch_clause', 'import_statement', 'operation_type' }
