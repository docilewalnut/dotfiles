require'nvim-treesitter.configs'.setup {
  ensure_installed = {
    "lua",
    "javascript",
    "typescript",
    "php",
    "css",
    "html"
  },

  auto_install = true,

  highlight = {
    enable = true,
    use_languagetree = true,
  },

  incremental_selection = {
    enable = true
  },

  indent = {
    enable = true,
  },
}
