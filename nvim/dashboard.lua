function splitStringByNewline(str)
    local result = {}
    for line in string.gmatch(str, "[^\r\n]+") do
        table.insert(result, line)
    end
    return result
end


local handle = io.popen("echo 'hi' | lolcat")
local textheader = handle:read("*a")
handle:close();

require'dashboard'.setup{
    theme = 'hyper', --  theme is doom and hyper default is hyper
    config = {
        header = { 
            -- textheader 
        },
        footer = {

        },
        shortcut_type = 'letter',
        shuffle_letter = false, 
        change_to_vcs_root = true, -- default is false,for open file in hyper mru. it will change to the root of vcs
        disable_move = true,
        packages = { enable = true },
        mru = { 
            limit = 10, 
            icon = '', 
            -- label = 'Recent files', 
            cwd_only = false 
        },
        shortcut = {
            { desc = '󰊳 Update', group = '@property', action = 'PlugUpdate', key = 'u' },
            {
              icon = ' ',
              icon_hl = '@variable',
              desc = 'Files',
              group = 'Label',
              action = 'Telescope find_files',
              key = 'f',
            },
            {
              desc = ' dotfiles ',
              group = 'Number',
              action = 'e ~/dotfiles',
              key = 'd',
            },
          },
    },    --  config used for theme
    hide = {
      statusline = true,   -- hide statusline default is true
      tabline = true, -- hide the tabline
      winbar = true -- hide winbar
    },
    preview = {
      -- command       -- preview command
      -- file_path     -- preview file path
      -- file_height   -- preview file height
      -- file_width    -- preview file width
    },
}
