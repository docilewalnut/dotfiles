local bufferline = require("bufferline")
bufferline.setup({
  options = {
    custom_areas = {
        right = function()
            local result = {}
            local seve = vim.diagnostic.severity
            local error = #vim.diagnostic.get(0, {severity = seve.ERROR})
            local warning = #vim.diagnostic.get(0, {severity = seve.WARN})
            local info = #vim.diagnostic.get(0, {severity = seve.INFO})
            local hint = #vim.diagnostic.get(0, {severity = seve.HINT})

            if error ~= 0 then
                table.insert(result, {text = "  " .. error, fg = "#EC5241"})
            end

            if warning ~= 0 then
                table.insert(result, {text = "  " .. warning, fg = "#EFB839"})
            end

            if hint ~= 0 then
                table.insert(result, {text = "  " .. hint, fg = "#A3BA5E"})
            end

            if info ~= 0 then
                table.insert(result, {text = "  " .. info, fg = "#7EA9A7"})
            end
            return result
        end,
    },
    mode = "tabs",
    style_preset = bufferline.style_preset.default,
    separator_style = "slant",
    indicator = { 
      style = 'icon'
    },
    buffer_close_icon = '',
    modified_icon = '●',
    close_icon = '',
    left_trunc_marker = '',
    right_trunc_marker = '',
    diagnostics = "nvim_lsp",
    diagnostics_indicator = function(count, level)
        local icon = level:match("error") and " " or "  "
        return " " .. icon .. count
    end,
    hover = {
      enabled = true,
      delay = 200,
      reveal = {'close'}
    },
    offsets = {
      {
        filetype="NvimTree",
        text="",
        highlight="Directory",
        separator=true
      }
    }
  }
})
