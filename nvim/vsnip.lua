
vim.g.vsnip_snippet_dir = '~/dotfiles/nvim/vsnip'

vim.g.vsnip_filetypes = {
    javascriptreact = {'javascript'},
    typescriptreact = {'typescript'}
}


