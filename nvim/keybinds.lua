local gs = package.loaded.gitsigns

local function map(mode, l, r, opts)
  opts = opts or {}
  opts.buffer = bufnr
  vim.keymap.set(mode, l, r, opts)
end

-- return zero-based position of visual selection
local get_visual = function()
  local curpos = vim.fn.getcurpos() 
  local one = { row = curpos[2] - 1, col = curpos[3] - 1 } 
  local two = { row = vim.fn.line('v') - 1, col = vim.fn.col('v') - 1 }

  if one.row == two.row then 
    if one.col > two.col then 
      local tmp = one 
      one = two 
      two = tmp 
    end 
  elseif one.row > two.row then
    local tmp = one 
    one = two 
    two = tmp 
  end

  two.col = two.col + 1
  return { start = one, ['end'] = two } 
end


-- UFO
vim.keymap.set('n', 'zR', require('ufo').openAllFolds)
vim.keymap.set('n', 'zM', require('ufo').closeAllFolds)
vim.keymap.set('n', 'zr', require('ufo').openFoldsExceptKinds)
vim.keymap.set('n', 'zm', require('ufo').closeFoldsWith) -- closeAllFolds == closeFoldsWith(0)
vim.keymap.set('n', 'K', function()
    local winid = require('ufo').peekFoldedLinesUnderCursor()
    if not winid then
        -- choose one of coc.nvim and nvim lsp
        vim.fn.CocActionAsync('definitionHover') -- coc.nvim
        vim.lsp.buf.hover()
    end
end)

-- Telescope
local telescope = require('telescope.builtin')
vim.keymap.set('n', '<C-E>', telescope.find_files, { noremap = true })
vim.keymap.set('n', '<C-F>', telescope.live_grep, { noremap = true })
vim.keymap.set('n', '<C-G>', telescope.lsp_document_symbols, { noremap = true })

vim.keymap.set('n', '<leader>ff', telescope.find_files, {})
vim.keymap.set('n', '<leader>fg', telescope.live_grep, {})
vim.keymap.set('n', '<leader>fb', telescope.buffers, {})
vim.keymap.set('n', '<leader>fh', telescope.help_tags, {})

vim.keymap.set('n', '<leader>gd', telescope.lsp_definitions, {})
vim.keymap.set('n', '<leader>gy', telescope.lsp_type_definitions, {})
vim.keymap.set('n', '<leader>gi', telescope.lsp_implementations, {})
vim.keymap.set('n', '<leader>gr', telescope.lsp_references, {})

vim.keymap.set('n', '<leader>fs', telescope.lsp_document_symbols, {})
vim.keymap.set('n', '<leader>fe', telescope.diagnostics, {})



-- Bufferline
local opts = { noremap = true, silent = true }
vim.keymap.set('n', '<A-,>', ':tabprev<CR>', opts)
vim.keymap.set('n', '<A-.>', ':tabnext<CR>', opts)
vim.keymap.set('n', '<A-n>', ':tabnew<CR>', opts)
vim.keymap.set('n', '<A-w>', ':tabclose<CR>', opts)


-- LSP
vim.keymap.set('n', '[g', vim.diagnostic.goto_prev, opts)
vim.keymap.set('n', ']g', vim.diagnostic.goto_next, opts)
vim.keymap.set('n', '<F2>', vim.lsp.buf.rename, opts)
vim.keymap.set('n', '<F12>', vim.lsp.buf.type_definition, opts)
vim.keymap.set('n', '<A-h>', '<C-O>', opts)
vim.keymap.set('n', '<A-l>', '<C-I>', opts)
-- vim.keymap.set('n', '<space>e', vim.diagnostic.open_float, opts)
vim.keymap.set('n', '[d', vim.diagnostic.goto_prev, opts)
-- vim.keymap.set('n', ']d', vim.diagnostic.goto_next, opts)
-- vim.keymap.set('n', '<space>q', vim.diagnostic.setloclist, opts)


-- Gitsigns
local gs = package.loaded.gitsigns

vim.keymap.set('n', ']c', function()
  if vim.wo.diff then return ']c' end
  vim.schedule(function() gs.next_hunk() end)
  return '<Ignore>'
end, {expr=true})

vim.keymap.set('n', '[c', function()
  if vim.wo.diff then return '[c' end
  vim.schedule(function() gs.prev_hunk() end)
  return '<Ignore>'
end, {expr=true})

vim.keymap.set({'n', 'v'}, '<leader>hs', ':Gitsigns stage_hunk<CR>')
vim.keymap.set({'n', 'v'}, '<leader>hr', ':Gitsigns reset_hunk<CR>')
vim.keymap.set({'o', 'x'}, '<leader>hS', function()
    local selStart, selEnd = unpack(get_visual())
    gs.stage_hunk({selStart, selEnd}, {greedy=false});
end)
vim.keymap.set('n', '<leader>hS', gs.stage_buffer)
vim.keymap.set('n', '<leader>h1', function() 
    local lineNum = vim.api.nvim_win_get_cursor(0)[1]
    gs.stage_hunk({lineNum,lineNum}, {greedy=false}) 
end)
vim.keymap.set('n', '<leader>hu', gs.undo_stage_hunk)
vim.keymap.set('n', '<leader>hR', gs.reset_buffer)
vim.keymap.set('n', '<leader>hp', gs.preview_hunk)
vim.keymap.set('n', '<leader>hb', function() gs.blame_line{full=true} end)
vim.keymap.set('n', '<leader>tb', gs.toggle_current_line_blame)
vim.keymap.set('n', '<leader>hd', gs.diffthis)
vim.keymap.set('n', '<leader>hD', function() gs.diffthis('~') end)
vim.keymap.set('n', '<leader>td', gs.toggle_deleted)
vim.keymap.set({'o', 'x'}, 'ih', ':<C-U>Gitsigns select_hunk<CR>')


-- nvim-tree
local nvim_tree = require("nvim-tree.api")
vim.keymap.set('n', '<leader>ni', nvim_tree.tree.toggle_hidden_filter)
vim.keymap.set('n', '<C-N>', nvim_tree.tree.toggle)

-- if has('mac') || has('macunix')

-- else
-- vim.keymap.set({'n', 'i'}, '<C-V>', )
vim.keymap.set('i', '<C-V>', '<ESC>"*pa')
vim.keymap.set('v', '<C-V>', ':c<ESC>"*p')

-- Bind commentary to C-/, needs _ as keycode
vim.keymap.set('i', '<C-_>', '<ESC>:Commentary<cr>i');
vim.keymap.set({'n', 'v'}, '<C-_>', ':Commentary<cr>')
-- vim.keymap.set('v', '<C-_>', ':Commentary<cr>')

vim.keymap.set('n', '<C-s>', ':w<cr>')
vim.keymap.set('n', '<C-s>', ':w<cr>')
-- fi
-- vsnip
-- local vsnip = require('vsnip')
-- vim.keymap.set({'i', 's'}, '<Tab>', function()
--     if vsnip.jumpable(1) then
--         vsnip.

-- end, {expr = true})
