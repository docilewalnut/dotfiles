function _prompt_char {
    git branch >/dev/null 2>/dev/null && echo '±' && return
    hg root >/dev/null 2>/dev/null && echo 'Hg' && return
    echo '○'
}

MODE_INDICATOR="%F{green}%K{green}%F{black} NORMAL "

function virtualenv_info {
    [ $VIRTUAL_ENV ] && echo '('`basename $VIRTUAL_ENV`') '
}

function git_prompt_slim_info() {
  ref=$(git symbolic-ref HEAD 2> /dev/null) || return
  echo "$(parse_git_dirty)$ZSH_THEME_GIT_PROMPT_PREFIX${ref#refs/heads/}$ZSH_THEME_GIT_PROMPT_SUFFIX"
}

PROMPT='
[ %F{red}%n%f %F{yellow}%m%f ] %F{blue}%~ %F{green}
$(_prompt_char) %# %b%s%f'

RPS1='$(vi_mode_prompt_info)$(git_prompt_slim_info)'

#

ZSH_THEME_GIT_PROMPT_PREFIX="%F{238}%K{238}%F{222}  "
ZSH_THEME_GIT_PROMPT_SUFFIX=" %K{235}%F{222} $(git_prompt_short_sha) %{$reset_color%}"
ZSH_THEME_GIT_PROMPT_DIRTY="%F{red}%K{red}%F{black} ✘ "
ZSH_THEME_GIT_PROMPT_UNTRACKED="%F{blue}%K{blue}%F{black} ? "
ZSH_THEME_GIT_PROMPT_CLEAN="%F{green}%K{green}%F{black} ✓ "
